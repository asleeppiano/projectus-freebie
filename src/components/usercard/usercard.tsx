import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

interface Props {
  username: string
  position: string
}

function Usercard({ username, position }: Props) {
  return (
    <div className="usercard-grid usercard_style">
      <img
        className="usercard-grid-photo usercard-photo_size"
        src="https://images.unsplash.com/photo-1570912262979-baf154050b3a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1534&q=80"
        alt="user"
      />
      <h2 className="usercard-grid-name usercard-name_style">{username}</h2>
      <p className="usercard-grid-position usercard-position_style">
        {position}
      </p>
      <button className="usercard-grid-button usercard-button_style">
        <FontAwesomeIcon color="rgba(155,155,155,1)" icon="ellipsis-h" />
      </button>
    </div>
  )
}
export default Usercard
