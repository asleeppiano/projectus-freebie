import React, { useRef, useEffect } from 'react'
import linearChart from '../../charts/linear'
import circularChart from '../../charts/circle'

type Props = {
  title: string
  info?: string
  cardType: string
grid?: string
}
type Data = {
  x: number
  y: number
}

function Infocard({ title, info, cardType, grid }: Props) {
  const chartRef = useRef<SVGSVGElement>(null!)
  const infocardRef = useRef<HTMLDivElement>(null!)
  const diagramRef = useRef<SVGSVGElement>(null!)

  useEffect(() => {
    let inst: any = undefined
    if (cardType === 'big') {
      inst = linearChart(chartRef.current, infocardRef.current)
    } else {
      inst = circularChart(diagramRef.current, infocardRef.current)
    }
    inst.generateChart()
    window.addEventListener('resize', () => {
    console.log(infocardRef.current.clientHeight)
      if (infocardRef.current.clientHeight < 300) inst.render()
    })
  }, [])

  return (
    <div
      ref={infocardRef}
      className={`infocard ${
        cardType === 'big' ? 'infocard_big' : 'infocard_small'
      } ${grid}`}
    >
      <h2 className={`infocard__title infocard-grid-title ${cardType === 'small' ? 'infocard__title_small' : ''}`}>{title}</h2>
      {cardType === 'big' ? (
        <>
          <div className="infocard__info infocard-grid-info">{info}</div>
          <div className="infocard__plot infocard-grid-plot">
            <svg ref={chartRef} className="infocard__chart"></svg>
          </div>
        </>
      ) : (
        <div className="infocard__plot">
          <svg ref={diagramRef} className="infocard__diagram"></svg>
        </div>
      )}
    </div>
  )
}
export default Infocard
