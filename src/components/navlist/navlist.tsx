import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import dashImg from '../../images/dash.png'

interface ListItem {
  id: string
  text: string
}

type Photo = {
  path: string
  name: string
}

// interface TeamsListItem extends ListItem {
//   photo: string
//   name: string
// }

interface Props {
  listType: string
  list: Array<ListItem>
  notifications?: number
  photos?: Array<Array<Photo>>
}

function menuListItem(item: ListItem, notifications: number) {
  return (
    <li key={item.id} className="navlist-items__item justify-start">
      <span className="navlist-items__text">{item.text}</span>
      {item.text === 'Notifications' ? (
        <div className="menu-item-notifications">
          <div>{notifications}</div>
        </div>
      ) : (
        ''
      )}
    </li>
  )
}
function projectsListItem(item: ListItem) {
  let img = 'dash'
  if (item.text == 'CRM System') {
    img = 'crm'
  } else if (item.text === 'Website Redesign') {
    img = 'web'
  } else if (item.text === 'Communication Tool') {
    img = 'comm'
  }

  return (
    <li key={item.id} className="navlist-items__item">
      <img src={dashImg} alt={item.text} />
      <div className="navlist-items__text navlist-items__text_pos">
        {item.text}
      </div>
      <button className="project-list-btn">
        <FontAwesomeIcon icon="ellipsis-h" />
      </button>
    </li>
  )
}
function teamsListItem(item: ListItem, photos: Array<Photo>) {
  return (
    <li key={item.id} className="navlist-items__item">
      <span className="navlist-items__text">{item.text}</span>
      <div className="navlist-items-teamslist-icons">
        {photos && photos.length > 0
          ? photos.map((photo: Photo) => (
            <div key={photo.path} className="navlist-items-teamslist-icons-wrapper">
              <img
                className="navlist-items-teamslist-icons__image"
                src={photo.path}
                alt={photo.name}
              />
            </div>
            ))
          : ''}
      </div>
    </li>
  )
}

function Navlist({ listType, list, notifications, photos }: Props) {
  let button = undefined
  if (listType === 'projects') {
    button = (
      <button className="navlist-items__add-button">+ Add a Project</button>
    )
  } else if (listType === 'teams') {
    button = <button className="navlist-items__add-button">+ Add a Team</button>
  }

  return (
    <nav className="navlist">
      <h3 className="navlist-type">{listType}</h3>
      <ul className={`navlist-items ${listType === 'menu' ? 'mb0' : ''}`}>
        {list && list.length > 0
          ? list.map((item, index) => {
              switch (listType) {
                case 'menu':
                  return menuListItem(item, notifications)
                case 'projects':
                  return projectsListItem(item)
                case 'teams':
                  return teamsListItem(item, photos[index])
                default:
                  return <span>no list</span>
              }
            })
          : ''}
      </ul>
      {button}
    </nav>
  )
}
export default Navlist
