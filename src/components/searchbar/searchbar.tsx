import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

interface Props {}

function Searchbar(props: Props) {
  const [input, setInput] = useState(false)
  function showInput() {
    setInput(input => (input ? false : true))
  }
  return (
    <div className="searchbar">
      <div className={`flex-start ${input ? 'none' : ''}`}>
        <img className="mr" src="../../images/logo.png" />
        <h1 className="searchbar__title">Projectus</h1>
      </div>
      <input
        className={`searchbar__input ${input ? 'searchbar__input_show' : ''}`}
        type="text"
        name="search"
      />
      <button onClick={showInput}
        className={`searchbar__close ${input ? 'searchbar__close_show' : ''}`}
      >
        <FontAwesomeIcon color="white" icon="times" />
      </button>
      <button
        className={`searchbar__button ${input ? 'none' : ''}`}
        onClick={showInput}
      >
        <FontAwesomeIcon color="darkgray" icon="search" />
      </button>
    </div>
  )
}
export default Searchbar
