import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

type Props = {
  title: string
}

function Addbutton({title}: Props) {
  return (
    <button className="addbutton">
      <FontAwesomeIcon icon="plus" />
      {title} 
    </button>
  )
}
export default Addbutton
