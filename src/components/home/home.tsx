import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Layout from '../layout/layout'
import Infocard from '../infocard/infocard'
import Taskscard from '../taskscard/taskscard'
import Activitycard from '../activitycard/activitycard'
import { getTasklistAction } from '../../actions/tasklist'
import { getActivitylistAction } from '../../actions/activity'
type Props = {}

function Home(props: Props) {
  const images = [
    {
      path:
        'https://images.unsplash.com/photo-1571637928227-e5ec14ecb8a0?ixlib=rb-1.2.1&auto=format&fit=crop&w=625&q=80',
      name: 'first',
    },
    {
      path:
        'https://images.unsplash.com/photo-1571637928003-0ed17e03786f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=382&q=80',
      name: 'second',
    },
    {
      path:
        'https://images.unsplash.com/photo-1547895749-c4209c6796b4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=80',
      name: 'third',
    },
    {
      path:
        'https://images.unsplash.com/photo-1570372225974-74fa85214b83?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=401&q=80',
      name: 'fourth',
    },
  ]

  const tasklist: any = useSelector((state: any) => state.getTasklist.tasklist)
  const activitylist: any = useSelector(
    (state: any) => state.getActivitylist.activitylist
  )

  console.log('ACTIVITY_LIST', activitylist)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getTasklistAction())
    dispatch(getActivitylistAction())
  }, [])

  return (
    <Layout name="Home">
      <div className="home">
        <Infocard
          grid="home-grid-big"
          cardType="big"
          title="Completed Tasks"
          info="372"
        />
        <div className="home-grid-small">
          <Infocard
            cardType="small"
            title="Working Rate"
          />
          <Infocard
            cardType="small"
            title="Performance"
          />
        </div>
        <Taskscard grid="home-grid-tasks" tasklist={tasklist} />
        <Activitycard
          grid="home-grid-activity"
          images={images}
          activities={activitylist}
        />
      </div>
    </Layout>
  )
}
export default Home
