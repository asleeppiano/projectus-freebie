import React from 'react'
import AddButton from '../addbutton/addbutton'
import { Task } from '../../types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

type Props = {
  tasklist: Array<Task>
  grid: string
}

function Taskscard({ tasklist, grid }: Props) {
  return (
    <div className={`taskscard ${grid}`}>
      <div className="taskscard__header">
        <h2>Today Tasks </h2>
        <AddButton title="AddTask" />
      </div>
      <ul className="taskscard-list">
        {tasklist && tasklist.length > 0
          ? tasklist.map((task, index) => (
              <li className="taskscard-list__task" key={index}>
                <div className="flex-start">
                  <div className="taskscard-list__done">
                    <FontAwesomeIcon icon="check" />
                  </div>
                  <h3>{task.text}</h3>
                </div>
                <div className="taskscard-list__meta">
                  <div className="taskscard-list__image">
                    <img
                      src="https://images.unsplash.com/photo-1537193282496-388b58cc6ee3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"
                      alt={task.assignee}
                    />
                  </div>
                  <div className="taskscard-list-taglist">
                    {task.tags.map(tag => (
                      <span className="taskscard-list-taglist__tag" key={tag}>
                        {' '}
                        {tag}{' '}
                      </span>
                    ))}
                  </div>
                </div>
              </li>
            ))
          : ''}
      </ul>
    </div>
  )
}
export default Taskscard
