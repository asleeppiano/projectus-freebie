import React from 'react'
import { Activity, Photo } from '../../types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { IconProp } from '@fortawesome/fontawesome-svg-core'

type Props = {
  activities: Array<Activity>
  images: Array<Photo>
  grid: string
}

type IconMap = {
  [key: string]: IconProp
}

const iconMap: IconMap = {
  done: 'check',
  upload: 'upload',
  comment: 'comment-alt',
  edit: 'pencil-alt',
}

function Activitycard({ activities, images, grid }: Props) {
  return (
    <div className={`activitycard ${grid}`}>
      <h2 className="activitycard__header"> Activity </h2>
      <div>
        <h4 className="activitycard__day"> TODAY </h4>
        {activities && activities.length > 0
          ? activities.slice(0, 3).map((activity: Activity) => {
              const date = new Date(activity.date)
              console.log('DATE', date)
              return (
                <div key={activity.id} className="activity">
                  <div
                    className={`activity__icon activity__icon_${activity.kind.toLowerCase()}`}
                  >
                    <FontAwesomeIcon icon={iconMap[activity.kind.toLowerCase()]} />
                  </div>
                  <div className="activity-content">
                    <p className="activity-content__text">{activity.text}</p>
                    <time
                      className="activity-content__date"
                      dateTime={activity.date.toString()}
                    >
                      {`${date.getHours()}:${date.getMinutes()}`}
                    </time>
                    {activity.kind === 'UPLOAD' ? (
                      <div className="activity-content-images">
                        {images && images.length > 0
                          ? images.map(image => (
                              <div key={image.path} className="activity-content-images__image">
                                <img src={image.path} alt={image.name} />
                              </div>
                            ))
                          : ''}
                      </div>
                    ) : (
                      ''
                    )}
                  </div>
                </div>
              )
            })
          : ''}
      </div>
      <div>
        <h4 className="activitycard__day"> YESTERDAY </h4>
        {activities && activities.length > 0
          ? activities.slice(3, 6).map((activity: Activity) => {
              const date = new Date(activity.date)
              console.log('DATE', date)
              return (
                <div key={activity.id} className="activity">
                  <div
                    className={`activity__icon activity__icon_${activity.kind.toLowerCase()}`}
                  >
                    <FontAwesomeIcon icon={iconMap[activity.kind.toLowerCase()]} />
                  </div>
                  <div className="activity-content">
                    <p className="activity-content__text">{activity.text}</p>
                    <time
                      className="activity-content__date"
                      dateTime={activity.date.toString()}
                    >
                      {`${date.getHours()}:${date.getMinutes()}`}
                    </time>
                    {activity.kind === 'UPLOAD' ? (
                      <div className="activity-content-images">
                        {images && images.length > 0
                          ? images.map(image => (
                              <div key={image.path} className="activity-content-images__image">
                                <img src={image.path} alt={image.name} />
                              </div>
                            ))
                          : ''}
                      </div>
                    ) : (
                      ''
                    )}
                  </div>
                </div>
              )
            })
          : ''}
      </div>
    </div>
  )
}
export default Activitycard
