import React from 'react'
import Header from '../header/header'
import Navigation from '../navigation/navigation'

interface Props {
  children: React.ReactNode
  name: string
}

function Layout({ children, name }: Props) {
  return (
    <div className="layout">
      <Header grid="layout-grid-header" name={name} />
      <Navigation grid="layout-grid-nav" />
      <div className="layout-grid-content">{children}</div>
    </div>
  )
}
export default Layout
