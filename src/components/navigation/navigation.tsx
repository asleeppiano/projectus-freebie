import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Searchbar from '../searchbar/searchbar'
import Usercard from '../usercard/usercard'
import NavList from '../navlist/navlist'
import nanoid from 'nanoid'
import { Photo } from '../../types'
import { getTasksAction } from '../../actions/tasks'
import { getNotificationsAction } from '../../actions/notifications'

type Props = {
  grid: string
}

function Navigation({ grid }: Props) {
  const menuNavList = [
    { id: nanoid(), text: 'Home' },
    { id: nanoid(), text: 'My Tasks' },
    { id: nanoid(), text: 'Notifications' },
  ]
  const projectsNavList = [
    { id: nanoid(), text: 'Dashboard UI Kit' },
    { id: nanoid(), text: 'CRM System' },
    { id: nanoid(), text: 'Website Redesign' },
    { id: nanoid(), text: 'Communication Tool' },
  ]
  const teamsNavList = [
    { id: nanoid(), text: 'Designers' },
    { id: nanoid(), text: 'Backend' },
    { id: nanoid(), text: 'Frontend' },
  ]
  const photos: Array<Array<Photo>> = [
    [
      {
        name: 'Mary',
        path:
          'https://images.unsplash.com/photo-1571088246690-8284cb626b06?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
      },
      {
        name: 'Danny',
        path:
          'https://images.unsplash.com/photo-1571193921548-6b0409657657?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjExMDk0fQ&auto=format&fit=crop&w=334&q=80',
      },
      {
        name: 'Liza',
        path:
          'https://images.unsplash.com/photo-1571252343909-abe38bc581d0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
      },
    ],
    [
      {
        name: 'Jane',
        path:
          'https://images.unsplash.com/photo-1568473883595-41ad577b1850?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80',
      },
      {
        name: 'Peter',
        path:
          'https://images.unsplash.com/photo-1569008476118-8d34d410077e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80',
      },
    ],
    [
      {
        name: 'Cory',
        path:
          'https://images.unsplash.com/photo-1563276054-27c77b7b50fe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=313&q=80',
      },
      {
        name: 'Alex',
        path:
          'https://images.unsplash.com/photo-1566412182247-a7344cb5b50d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80',
      },
      {
        name: 'Diana',
        path:
          'https://images.unsplash.com/photo-1568473405063-e0104d904ad5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=332&q=80',
      },
      {
        name: 'Rebekka',
        path:
          'https://images.unsplash.com/photo-1547725637-97d078cac12e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
      },
    ],
  ]
  const dispatch = useDispatch()
  const tasks: any = useSelector((state: any) => state.getTasks.tasks)
  const notifications: any = useSelector(
    (state: any) => state.getNotifications.notifications.num
  )

  useEffect(() => {
    dispatch(getTasksAction())
    dispatch(getNotificationsAction())
  }, [])

  return (
    <div className={`nav ${grid}`}>
      <div className="nav-content">
        <Searchbar />
        <Usercard username="Emilee Simchenko" position="Product Owner" />
        <div className="nav-tasks">
          <div className="nav-tasks__completed">
            <div className="nav-tasks__completed_num">{tasks.completed}</div>
            <div className="nav-tasks__completed_text">Completed Tasks</div>
          </div>
          <div className="nav-tasks__open">
            <div className="nav-tasks__completed_num">{tasks.open}</div>
            <div className="nav-tasks__completed_text">Open Tasks</div>
          </div>
        </div>
        <NavList
          listType="menu"
          notifications={notifications}
          list={menuNavList}
        />
        <NavList listType="projects" list={projectsNavList} />
        <NavList listType="teams" photos={photos} list={teamsNavList} />
      </div>
      <div className="nav-collab">
        {' '}
        <a className="nav-collab__link" href="/">
          Invite your team
        </a>{' '}
        and start collaborating
      </div>
    </div>
  )
}
export default Navigation
