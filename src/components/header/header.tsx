import React from 'react'
import AddButton from '../addbutton/addbutton'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

type Props = {
  name: string
  grid: string
}

function Header({ name, grid }: Props) {
  return (
    <div className={`header ${grid}`}>
      <div className="header-right">
        <h1 className="header-right__title">{name}</h1>
        <button className="header-right__button">
          <FontAwesomeIcon icon="ellipsis-h" />
        </button>
      </div>
      <div className="header-left">
        <AddButton title="Add" />
      </div>
    </div>
  )
}
export default Header
