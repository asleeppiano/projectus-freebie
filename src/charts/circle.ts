import * as d3 from 'd3'

export default function circularChart(
  chartRef: SVGSVGElement,
  infocardRef: HTMLDivElement
) {
  let radius = 50
  let thickness = 7
  let arc: any = undefined
  let arc2: any = undefined
  let arc3: any = undefined
  let arc4: any = undefined
  let svg: any = undefined
  let x: any = undefined
  let y: any = undefined
  let path: any = undefined
  let bg: any = undefined
  let fg: any = undefined
  let fg2: any = undefined
  let fg3: any = undefined
  let fg4: any = undefined
  let label: any = undefined

  function generateChart() {

    svg = d3.select(chartRef)
      .append('g')
      .attr('transform', `translate(${radius}, ${radius})`)
      .attr('class', 'circle-group')

    arc = d3
      .arc()
      .innerRadius(radius - thickness)
      .outerRadius(radius)
      .startAngle(0)

    bg = svg.append('path')
      .datum({ endAngle: 2 * Math.PI  })
      .attr('class', 'circle-background')
      .attr('d', arc)

    fg = svg.append('path')
      .datum({ endAngle: Math.PI / 6  })
      .attr('class', 'circle-foreground')
      .attr('d', arc)

    arc2 = d3
      .arc()
      .innerRadius(radius - thickness)
      .outerRadius(radius)
      .startAngle(Math.PI / 2)

    fg2 = svg.append('path')
      .datum({endAngle: 2 * Math.PI / 3})
      .attr('class', 'circle-foreground')
      .attr('d', arc2)

    arc3 = d3 
      .arc()
      .innerRadius(radius - thickness)
      .outerRadius(radius)
      .startAngle(Math.PI)

    fg3 = svg.append('path')
      .datum({ endAngle: 7 * Math.PI / 6 })
      .attr('class', 'circle-foreground')
      .attr('d', arc3)

    arc4 = d3
      .arc()
      .innerRadius(radius - thickness)
      .outerRadius(radius)
      .startAngle(3 * Math.PI / 2)

    fg4 = svg.append('path')
      .datum({ endAngle: 5 * Math.PI / 3 })
      .attr('class', 'circle-foreground')
      .attr('d', arc4)
    

    label = svg.append('text')
      .attr('class', 'circle-label')
      .attr('x', 2)
      .attr('y', 2)
    .attr('dominant-baseline', 'middle')

    render()
  }

  function updateDimensions(cliWidth: number) {
    radius = cliWidth * 0.5 / 2.2
  }

  function render() {
    updateDimensions(infocardRef.clientWidth)

    arc = d3
      .arc()
      .innerRadius(radius - thickness)
      .outerRadius(radius)
      .startAngle(0)

    bg = bg
      .attr('d', arc)

    fg = fg.attr('d', arc)

    arc2 = d3
      .arc()
      .innerRadius(radius - thickness)
      .outerRadius(radius)
      .startAngle(Math.PI / 2)

    fg2 = fg2.attr('d', arc2)

    arc3 = d3 
      .arc()
      .innerRadius(radius - thickness)
      .outerRadius(radius)
      .startAngle(Math.PI)

    fg3 = fg3.attr('d', arc3)

    arc4 = d3
      .arc()
      .innerRadius(radius - thickness)
      .outerRadius(radius)
      .startAngle(3 * Math.PI / 2)

    fg4 = fg4.attr('d', arc4)

    let width = radius * 2
    
    chartRef 
      .setAttribute('width', width.toString())

    chartRef
      .setAttribute('height', width.toString())

    d3.selectAll('.circle-group')
      .attr('transform', `translate(${radius}, ${radius})`)

    console.log('select', d3.selectAll('.circle-group'))

    label.text('75%')
  }
  return {
    generateChart,
    render,
  }
}
