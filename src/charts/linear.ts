import * as d3 from 'd3'

export default function linearChart(chartRef: SVGSVGElement, infocardRef: HTMLDivElement) {
  let width = 400
  let height = 250
  let lineFunc: any = undefined
  let svg: any = undefined
  let x: any = undefined
  let y: any = undefined
  let path: any = undefined
  const data: any = [
    { x: 0, y: 200 },
    { x: 50, y: 100 },
    { x: 100, y: 130 },
    { x: 150, y: 30 },
    { x: 200, y: 120 },
    { x: 250, y: 200 },
    { x: 270, y: 190 },
    { x: 300, y: 190 },
    { x: 350, y: 230 },
    { x: 400, y: 180 },
  ]
  function generateChart() {
    let xExtent: any = d3.extent(data, (d: any) => d.x)
    let yExtent: any = d3.extent(data, (d: any) => d.y)

    x = d3.scaleLinear().domain(xExtent) // scale
    y = d3.scaleLinear().domain(yExtent) // scale

    svg = d3.select(chartRef)

    lineFunc = d3
      .line()
      .curve(d3.curveBasis)
      .x((d: any) => {
        return x(d.x)
      })
      .y((d: any) => {
        return y(d.y)
      })

    path = svg
      .append('path')
      .datum(data)
      .attr('class', 'infocard__chart')

    render()
  }

  function updateDimensions(cliWidth: number) {
    width = cliWidth * 0.5
    height = 0.3 * width
  }

  function render() {
    updateDimensions(infocardRef.clientWidth)

    x.range([0, width])
    y.range([0, height])
    console.log('range', x, y)

    svg.attr('width', width).attr('height', height)

    path.attr('d', lineFunc)
  }
  return {
    generateChart,
    render,
  }
}
