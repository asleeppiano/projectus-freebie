import React from 'react'
import './App.scss'
import { Provider } from 'react-redux'
import store from './store'
// import { BrowserRouter as Router, Route } from 'react-router-dom'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faSearch,
  faEllipsisH,
  faTimes,
  faPlus,
  faCheck,
  faCommentAlt,
  faUpload,
  faPencilAlt,
} from '@fortawesome/free-solid-svg-icons'
import Home from './components/home/home'

library.add(
  faSearch,
  faEllipsisH,
  faTimes,
  faPlus,
  faCheck,
  faCommentAlt,
  faUpload,
  faPencilAlt
)

function App() {
  return (
    <Provider store={store}>
      <Home />
    </Provider>
  )
}

export default App
