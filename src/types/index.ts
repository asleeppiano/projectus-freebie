export interface Tasks {
  readonly completed: number
  readonly open: number
}

export interface Notifications {
  readonly num: number
}

export interface Photo {
  readonly path: string
  readonly name: string
}

export interface Task {
  readonly text: string
  readonly tags: Array<string>
  readonly assignee: string
}

export enum ActivityKind {
  DONE = 'DONE',
  UPLOAD = 'UPLOAD',
  COMMENT = 'COMMENT',
  EDIT = 'EDIT'
}

export interface Activity {
  readonly id: string
  readonly text: string
  readonly date: Date
  readonly kind: ActivityKind
}
