import { ActivityAction } from '../actions/activity'
import {
  GET_ACTIVITYLIST_ERROR,
  ASYNC_START,
  GET_ACTIVITYLIST_SUCCESS,
} from '../constants'
import { Activity } from '../types'

type State = {
  isFetching: boolean
  activitylist: Array<Activity>
  error: Error | null
}

const initialState: State = {
  isFetching: false,
  activitylist: [],
  error: null,
}

export default function getActivitylist(state = initialState, action: ActivityAction) {
  switch (action.type) {
    case ASYNC_START:
      return { ...state, isFetching: true }
    case GET_ACTIVITYLIST_ERROR:
      return { ...state, error: action.error }
    case GET_ACTIVITYLIST_SUCCESS:
      return { ...state, activitylist: action.activitylist }
    default:
      return state
  }
}
