import { TasksAction } from '../actions/tasks'
import {
  GET_TASKS_ERROR,
  ASYNC_START,
  GET_TASKS_SUCCESS,
} from '../constants'
import { Tasks } from '../types'

type State = {
  isFetching: boolean
  tasks: Tasks
  error: Error | null
}

const initialState: State = {
  isFetching: false,
  tasks: { completed: 0, open: 0 },
  error: null,
}

export default function getTasks(state = initialState, action: TasksAction) {
  switch (action.type) {
    case ASYNC_START:
      return { ...state, isFetching: true }
    case GET_TASKS_ERROR:
      return { ...state, error: action.error }
    case GET_TASKS_SUCCESS:
      return { ...state, tasks: action.tasks }
    default:
      return state
  }
}
