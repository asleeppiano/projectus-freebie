import { combineReducers } from 'redux'
import getTasks from './tasks'
import getNotifications from './notifications'
import getTasklist from './tasklist'
import getActivitylist from './activity'

export default combineReducers({
  getTasks,
  getNotifications,
  getTasklist,
  getActivitylist
})
