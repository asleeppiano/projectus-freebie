import { NotificationsAction } from '../actions/notifications'
import {
  ASYNC_START,
  GET_NOTIFICATIONS_ERROR,
  GET_NOTIFICATIONS_SUCCESS,
} from '../constants'
import { Notifications } from '../types'

type State = {
  isFetching: boolean
  notifications: Notifications
  error: Error | null
}

const initialState: State = {
  isFetching: false,
  notifications: { num: 0 },
  error: null,
}

export default function getNotifications(state = initialState, action: NotificationsAction) {
  switch (action.type) {
    case ASYNC_START:
      return { ...state, isFetching: true }
    case GET_NOTIFICATIONS_ERROR:
      return { ...state, error: action.error }
    case GET_NOTIFICATIONS_SUCCESS:
      return { ...state, notifications: action.notifications }
    default:
      return state
  }
}

