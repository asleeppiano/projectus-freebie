import { TasklistAction } from '../actions/tasklist'
import {
  ASYNC_START,
  GET_TASKLIST_ERROR,
  GET_TASKLIST_SUCCESS,
} from '../constants'
import { Task } from '../types'

type State = {
  isFetching: boolean
  tasklist: Array<Task>
  error: Error | null
}

const initialState: State = {
  isFetching: false,
  tasklist: [],
  error: null,
}

export default function getTasklist(
  state = initialState,
  action: TasklistAction
) {
  switch (action.type) {
    case ASYNC_START:
      return { ...state, isFetching: true }
    case GET_TASKLIST_ERROR:
      return { ...state, error: action.error }
    case GET_TASKLIST_SUCCESS:
      return { tasklist: action.tasklist }
    default:
      return state
  }
}
