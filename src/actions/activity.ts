import {
  API_ADDR,
  GET_ACTIVITYLIST_ERROR,
  GET_ACTIVITYLIST_SUCCESS,
} from '../constants'
import { AsyncStart } from './async'
import { Activity } from '../types'
import { Dispatch } from 'redux'


export interface GetActivitylistError {
  type: GET_ACTIVITYLIST_ERROR
  error: Error
}
export interface GetActivitylistSuccess {
  type: GET_ACTIVITYLIST_SUCCESS
  activitylist: Array<Activity>
}
export type ActivityAction = AsyncStart | GetActivitylistSuccess | GetActivitylistError

export function GetActivitylistError(error: Error): GetActivitylistError {
  return {
    type: GET_ACTIVITYLIST_ERROR,
    error,
  }
}
export function GetActivitylistSuccess(activitylist: Array<Activity>): GetActivitylistSuccess {
  return {
    type: GET_ACTIVITYLIST_SUCCESS,
    activitylist,
  }
}

export function getActivitylistAction() {
  return (dispatch: Dispatch<ActivityAction>) => {
    dispatch(AsyncStart())

    fetch(API_ADDR + 'api/activitylist', {
      mode: 'cors',
    })
      .then(res => res.json())
      .then(json => dispatch(GetActivitylistSuccess(json)))
      .catch(err => dispatch(GetActivitylistError(err)))
  }
}
