import {
  API_ADDR,
  GET_NOTIFICATIONS_ERROR,
  GET_NOTIFICATIONS_SUCCESS,
} from '../constants'
import { AsyncStart } from './async'
import { Notifications } from '../types'
import { Dispatch } from 'redux'


export interface GetNotificationsError { type: GET_NOTIFICATIONS_ERROR
  error: Error
}
export interface GetNotificationsSuccess {
  type: GET_NOTIFICATIONS_SUCCESS
  notifications: Notifications  
}
export type NotificationsAction = AsyncStart | GetNotificationsError | GetNotificationsSuccess

export function GetNotificationsError(error: Error): GetNotificationsError {
  return {
    type: GET_NOTIFICATIONS_ERROR,
    error,
  }
}
export function GetNotificationsSuccess(notifications: Notifications): GetNotificationsSuccess {
  return {
    type: GET_NOTIFICATIONS_SUCCESS,
    notifications,
  }
}

export function getNotificationsAction() {
  return (dispatch: Dispatch<NotificationsAction>) => {
    dispatch(AsyncStart())

    fetch(API_ADDR + 'api/notifications', {
      mode: 'cors',
    })
      .then(res => res.text())
      .then(text => dispatch(GetNotificationsSuccess({num: parseInt(text)})))
      .catch(err => dispatch(GetNotificationsError(err)))
  }
}
