import { ASYNC_START } from '../constants'

export interface AsyncStart {
  type: ASYNC_START
}

export function AsyncStart(): AsyncStart {
  return {
    type: ASYNC_START,
  }
}
