import {
  API_ADDR,
  GET_TASKLIST_SUCCESS,
  GET_TASKLIST_ERROR,
} from '../constants'
import { AsyncStart } from './async'
import { Task } from '../types'
import { Dispatch } from 'redux'


export interface GetTasklistError {
  type: GET_TASKLIST_ERROR
  error: Error
}
export interface GetTasklistSuccess {
  type: GET_TASKLIST_SUCCESS
  tasklist: Array<Task>
}

export type TasklistAction = AsyncStart | GetTasklistSuccess | GetTasklistError

export function GetTasklistError(error: Error): GetTasklistError {
  return {
    type: GET_TASKLIST_ERROR,
    error,
  }
}
export function GetTasklistSuccess(tasklist: Array<Task>): GetTasklistSuccess {
  return {
    type: GET_TASKLIST_SUCCESS,
    tasklist,
  }
}

export function getTasklistAction() {
  return (dispatch: Dispatch<TasklistAction>) => {
    dispatch(AsyncStart())

    fetch(API_ADDR + 'api/tasklist', {
      mode: 'cors',
    })
      .then(res => res.json())
      .then(json => dispatch(GetTasklistSuccess(json)))
      .catch(err => dispatch(GetTasklistError(err)))
  }
}
