import {
  API_ADDR,
  GET_PHOTOS_ERROR,
  GET_PHOTOS_SUCCESS,
} from '../constants'
import { AsyncStart } from './async'
import { Photos } from '../types'
import { Dispatch } from 'redux'

export interface GetPhotosError {
  type: GET_PHOTOS_ERROR
  error: Error
}
export interface GetPhotosSuccess {
  type: GET_PHOTOS_SUCCESS
  photos: Array<Photos>
}
export type GetPhotosAction = AsyncStart | GetPhotosSuccess | GetPhotosSuccess

export function GetPhotosError(error: Error): GetPhotosError {
  return {
    type: GET_PHOTOS_ERROR,
    error,
  }
}
export function GetPhotosSuccess(photos: Array<Photos>): GetPhotosSuccess {
  return {
    type: GET_PHOTOS_SUCCESS,
    photos,
  }
}

// export function getNotificationsAction() {
//   return (dispatch: Dispatch<GetPhotosAction>) => {
//     dispatch(AsyncStart())

//     fetch(API_ADDR + 'api/notifications', {
//       mode: 'cors',
//     })
//       .then(res => res.text())
//       .then(text => dispatch(GetTasksSuccess({num: parseInt(text)})))
//       .catch(err => dispatch(GetTasksError(err)))
//   }
// }
