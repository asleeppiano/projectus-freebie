import {
  API_ADDR,
  GET_TASKS_ERROR,
  GET_TASKS_SUCCESS,
} from '../constants'
import { AsyncStart } from './async'
import { Tasks } from '../types'
import { Dispatch } from 'redux'


export interface GetTasksError {
  type: GET_TASKS_ERROR
  error: Error
}
export interface GetTasksSuccess {
  type: GET_TASKS_SUCCESS
  tasks: Tasks
}
export type TasksAction = AsyncStart | GetTasksSuccess | GetTasksError

export function GetTasksError(error: Error): GetTasksError {
  return {
    type: GET_TASKS_ERROR,
    error,
  }
}
export function GetTasksSuccess(tasks: Tasks): GetTasksSuccess {
  return {
    type: GET_TASKS_SUCCESS,
    tasks,
  }
}

export function getTasksAction() {
  return (dispatch: Dispatch<TasksAction>) => {
    dispatch(AsyncStart())

    fetch(API_ADDR + 'api/tasks', {
      mode: 'cors',
    })
      .then(res => res.json())
      .then(json => dispatch(GetTasksSuccess(json)))
      .catch(err => dispatch(GetTasksError(err)))
  }
}
